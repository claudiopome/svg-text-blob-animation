const colors = ['#632ddd','#6defb4','#fad949', '#57cce8', '#ed3824', '#ef642b', '#171668'];
var blobs = document.querySelectorAll("path");
blobs.forEach(blob => {
  blob.style.fill = colors[Math.floor(Math.random() * colors.length)];
});